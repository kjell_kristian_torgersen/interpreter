//============================================================================
// Name        : interpreter.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

#include "Evaluator.hpp"

using namespace std;

int main() {
	Evaluator e;
	string statement;
	do {
		cin >> statement;
		if (statement != "quit") {
			e.Evaluate(statement);
		}
	} while (statement != "quit");
	return 0;
}
