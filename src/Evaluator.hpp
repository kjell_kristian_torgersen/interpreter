/*
 * Evaluator.hpp
 *
 *  Created on: May 6, 2017
 *      Author: kjell
 */

#ifndef EVALUATOR_HPP_
#define EVALUATOR_HPP_

#include <gmpxx.h>
#include <map>

class Evaluator {
public:
	void Evaluate(std::string statement);
	virtual ~Evaluator();
private:
	std::map<std::string,mpq_class> vars;
	bool IsWhite(char c);
	char Getc();
	char Peek();
	void Match(char c);
	std::string GetVar();
	bool IsNumber(char c);
	bool IsAlpha(char c);
	bool IsVar(char c);
	mpq_class GetNum();
	mpq_class Expression();
	mpq_class Term();
	mpq_class Factor();
	void Error(std::string message);

	std::string statement;
	int idx;
};

#endif /* EVALUATOR_HPP_ */
