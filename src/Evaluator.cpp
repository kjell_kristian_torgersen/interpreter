/*
 * Evaluator.cpp
 *
 *  Created on: May 6, 2017
 *      Author: kjell
 */

#include <iostream>
#include "Evaluator.hpp"

void Evaluator::Evaluate(std::string statement) {
	this->statement = statement;
	this->idx = 0;

	if (statement == "vars") {
		for (auto& var : vars) {
			std::cout << var.first << " = " << var.second << std::endl;
		}
	} else {
		size_t s = statement.find('=');
		if (s == statement.npos) {
			mpq_class result = Expression();
			vars["ans"] = result;
			std::cout << " = " << result << " (" << result.get_d() << ")"
					<< std::endl;
		} else {
			mpq_class result;
			std::string var = GetVar();
			//std::cout << "var: " << var << std::endl;
			Match('=');
			result = Expression();
			vars[var] = result;
			vars["ans"] = result;
			std::cout << var << " = " << result << " (" << result.get_d() << ")"
					<< std::endl;
		}
	}
}

Evaluator::~Evaluator() {
	// TODO Auto-generated destructor stub
}

char Evaluator::Getc() {
	char c;
	do {
		c = statement[idx++];
	} while (IsWhite(c));
	return c;
}

char Evaluator::Peek() {
	char c = statement[idx];
	while (IsWhite(c))
		c = statement[++idx];
	return c;
}

void Evaluator::Match(char c) {
	char got = Getc();
	if (c != got) {
		std::cerr << "Parse error: Expected " << c << ", but got " << got
				<< std::endl;
	}
}

bool Evaluator::IsNumber(char c) {
	if (c == '.')
		return true;
	if (c >= '0' && c <= '9')
		return true;
	return false;
}

mpq_class Evaluator::GetNum() {
	mpq_class n = 0.0;
	char c;
	bool dot = false;
	int k = 0;
	mpq_class neg = 1.0;
	if (Peek() == '-') {
		neg = -1.0;
		Match('-');
	}
	while (IsNumber(Peek())) {
		c = Getc();
		if (c != '.') {
			n *= 10;
			n += (double) (c - '0');
		} else {
			dot = true;
		}
		if (dot)
			k++;
	}

	while (--k > 0)
		n /= 10.0;

	return neg * n;
}

mpq_class Evaluator::Expression() {
	mpq_class val = Term();
	while (Peek() == '+' || Peek() == '-') {
		switch (Peek()) {
		case '+':
			Match('+');
			if (Peek() == '-') {
				Match('-');
				val = val - Term();
			} else {
				val = val + Term();
			}
			break;
		case '-':
			Match('-');
			if (Peek() == '-') {
				Match('-');
				val = val + Term();
			} else {
				val = val - Term();
			}
			break;
		default:
			break;
		}
	}
	return val;
}

mpq_class Evaluator::Term() {
	mpq_class val = Factor();
	while (Peek() == '*' || Peek() == '/') {
		switch (Peek()) {
		case '*':
			Match('*');
			val = val * Factor();
			break;
		case '/':
			Match('/');
			val = val / Factor();
			break;
		default:
			break;
		}

	}
	return val;
}

mpq_class Evaluator::Factor() {
	mpq_class val;
	if (Peek() == '(') {
		Match('(');
		val = Expression();
		Match(')');
	} else if (IsNumber(Peek())) {
		val = GetNum();
	} else if (IsAlpha(Peek())) {
		std::string var = GetVar();
		auto it = vars.find(var);
		if (it == vars.end()) {
			Error("Variable " + var + " not found");
			val = 0;
		} else {
			val = vars[var];
		}
	} else {
		val = 0.0;
		Error("Error");
	}
	return val;
}

bool Evaluator::IsWhite(char c) {
	if (c == ' ' || c == '\t' || c == '\r' || c == '\n')
		return true;
	else
		return false;
}

bool Evaluator::IsAlpha(char c) {
	if (c >= 'A' && c <= 'Z')
		return true;
	if (c >= 'a' && c <= 'z')
		return true;
	return false;
}

bool Evaluator::IsVar(char c) {
	if (c >= 'A' && c <= 'Z')
		return true;
	if (c >= 'a' && c <= 'z')
		return true;
	if (c >= '0' && c <= '9')
		return true;
	if (c == '_')
		return true;
	return false;
}

std::string Evaluator::GetVar() {
	std::string var;
	char c;
	while (IsVar(c = Peek())) {
		Match(c);
		var += c;
	}
	return var;
}

void Evaluator::Error(std::string message) {
	std::cerr << message << std::endl;
}
